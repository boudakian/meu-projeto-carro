public class Carro {

    public static final String VERMELHO = "vermelho";
    public static final String PRETA = "preta";

    private Integer quantidadePneus;
    private Integer quantidadeCalotas;
    private Integer quantidadeParafusosPneu;
    private String cor;
    private Integer quantidadePortas;
    private String numeroChaci;
    private Integer anoFabricação;
    private String tipoGasolina;
    



    public String getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(String tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public Integer getAnoFabricação() {
        return anoFabricação ;
    }

    public void setAnoFabricação(Integer anoFabricação) {
        this.anoFabricação = 2019;
    }

    public String getNumeroChaci() {
        return numeroChaci ;
    }

    public void setNumeroChaci(String numeroChaci) {
        this.numeroChaci = numeroChaci ;
    }

    public Integer getQuantidadePortas() {
        return quantidadePortas ;
    }

    public void setQuantidadePortas(Integer quantidadePortas) {
        
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getCor() {
        return cor;
    }

    public Carro(Integer quantidadePneus){
        this.quantidadePneus = quantidadePneus; // this = acesso da variavel no escopo de classe quanto temos dentro de um metodo ou construtor um mesmo nome acessamos pela palavra "this".
        setQuantidadePneus(quantidadePneus);
    } // construtor defaut que o proprio java gera.

    public Integer getQuantidadePneus(){ // metodo para acrescentar mais um valor "get"(que seria o step do pneus"). O metodo get acessa aquela variavel.
        return quantidadePneus + 2;
    }

    public void setQuantidadePneus(Integer quantidadePneus){
       setQuantidadeCalotas (quantidadePneus);
        quantidadeParafusosPneu = quantidadePneus * 4 ;
        this.quantidadePneus = quantidadePneus;
        this.quantidadePortas = 4;
        this.numeroChaci = ("9BG ME70RA 9 B194503");
        this.anoFabricação = 2019;
        this.tipoGasolina = ("Gasolina aditivada");
    } // criamos uma regra na qual que sempre informar-mos a quantidade de pneus ja informamos a quantidade de calotas.


    public Integer getQuantidadeCalotas() {
        return quantidadeCalotas;
    }

    public void setQuantidadeCalotas(Integer quantidadeCalotas) {
        this.quantidadeCalotas = quantidadeCalotas;
    }

    public Integer getQuantidadeParafusosPneu() {
        return quantidadeParafusosPneu;
    }

    public void setQuantidadeParafusosPneu(Integer quantidadeParafusosPneu) {
        this.quantidadeParafusosPneu = quantidadeParafusosPneu;
    }

    public void getQuantidadePortas(Integer quantidadePortas){
        this.quantidadePortas = quantidadePortas;
    }


    public void imprimeValores(){
       System.out.println("Quantidade de Pneus " + getQuantidadePneus()                       ); // imprimir a quantidade de pneus que aparecera na tela... quantidadePneus e igual o valor 4.
       System.out.println("Quantidade de Calotas " + getQuantidadeCalotas()                   );
       System.out.println("Quantidade de Parafusos dos Pneus " + getQuantidadeParafusosPneu() );
       System.out.println("Cor " + getCor()                                                   );
       System.out.println("Quantidade de Portas " + getQuantidadePortas()                     );  
       System.out.println("Numeração do Chaci " + getNumeroChaci()                            );
       System.out.println("Ano de Fabricação " + getAnoFabricação()                           );
       System.out.println("Tipo de Gasolina " + getTipoGasolina()                             );
    }
}
